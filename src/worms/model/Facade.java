package worms.model;

public class Facade implements IFacade {

	/**
	 * 
	 */
	@Override
	public Worm createWorm(double x, double y, double direction, double radius,
			String name) throws ModelException {
		
		Worm worm;
		try {
			worm = new Worm(x, y, direction, radius, name);
		} catch (IllegalArgumentException exception) {
			throw new ModelException(exception);
		}
		
		return worm;
	}
	
	/**
	 * 
	 */
	@Override
	public boolean canMove(Worm worm, int nbSteps) {
		return worm.canMove(nbSteps);
	}
	
	/**
	 * 
	 */
	@Override
	public void move(Worm worm, int nbSteps) throws ModelException {
		try {
			worm.move(nbSteps);
		} catch (IllegalArgumentException exception) {
			throw new ModelException(exception);
		}
	}

	/**
	 * 
	 */
	@Override
	public boolean canTurn(Worm worm, double angle) {
		return worm.canTurn(angle);
	}
	
	/**
	 * 
	 */
	@Override
	public void turn(Worm worm, double angle) {
		worm.turn(angle);
	}

	/**
	 * 
	 */
	@Override
	public void jump(Worm worm) throws ModelException {
		try {
			worm.jump();
		} catch (IllegalStateException exception) {
			throw new ModelException(exception);
		}
	}

	/**
	 * 
	 */
	@Override
	public double getJumpTime(Worm worm) {
		return worm.getJumpTime();
	}

	/**
	 * 
	 */
	@Override
	public double[] getJumpStep(Worm worm, double t) throws ModelException {
		try {
			return worm.getJumpStep(t);
		} catch (IllegalArgumentException exception) {
			throw new ModelException(exception);
		}
	}

	/**
	 * 
	 */
	@Override
	public double getX(Worm worm) {
		return worm.getXCoordinate();
	}

	/**
	 * 
	 */
	@Override
	public double getY(Worm worm) {
		return worm.getYCoordinate();
	}

	/**
	 * 
	 */
	@Override
	public double getOrientation(Worm worm) {
		return worm.getOrientation();
	}

	/**
	 * 
	 */
	@Override
	public double getRadius(Worm worm) {
		return worm.getRadius();
	}

	/**
	 * 
	 */
	@Override
	public void setRadius(Worm worm, double newRadius) throws ModelException {
		try {
			worm.setRadius(newRadius);
		} catch (IllegalArgumentException exception) {
			throw new ModelException(exception);
		}
	}
	
	/**
	 * 
	 */
	@Override
	public double getMinimalRadius(Worm worm) {
		return worm.getMinimalRadius();
	}

	/**
	 * 
	 */
	@Override
	public int getActionPoints(Worm worm) {
		return worm.getActionPoints();
	}

	/**
	 * 
	 */
	@Override
	public int getMaxActionPoints(Worm worm) {
		return worm.getMaxActionPoints();
	}

	/**
	 * 
	 */
	@Override
	public String getName(Worm worm) {
		return worm.getName();
	}

	/**
	 * 
	 */
	@Override
	public void rename(Worm worm, String newName) throws ModelException {
		try {
			worm.setName(newName);
		} catch (IllegalArgumentException exception) {
			throw new ModelException(exception);
		}
	}

	/**
	 * 
	 */
	@Override
	public double getMass(Worm worm) {
		return worm.getMass();
	}

}
