package worms.model;

import be.kuleuven.cs.som.annotate.Value;


@Value
public enum Actions {
	MOVE {

		@Override
		public double getRequiredActionPoints(Worm worm) {
			return Math.abs(Math.cos(worm.getOrientation()) 
					 + Math.abs(4*Math.sin(worm.getOrientation())));
		}
		
	} ,
	
	TURN {

		@Override
		public double getRequiredActionPoints(Worm worm) {
			return (60/worm.getOrientation());
		}
		
	};
	
	public abstract double getRequiredActionPoints(Worm worm);
	
}
