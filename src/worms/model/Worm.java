package worms.model;
import be.kuleuven.cs.som.annotate.*;
/**
 * @invar	A worms position is a valid position.
 * 			| isValidXCoordinate(getXCoordinate) 
 * 			|	&& isValidYCoordinate(getYCoordinate)
 * @invar	A worms orientation is a valid orientation.
 * 			| isValidOrientation(getOrientation())
 * @invar	A worms radius is a valid radius.
 * 			| isValidRadius(getRadius(), this)
 * @invar	A worms name is a valid name.
 * 			| isValidName(getName())
 * @invar	A worm has a valid amount of actionpoints.
 * 			| isValidActionPoints(getActionPoints(), this)
 * @invar	The minimal radius of a worm is larger than zero.
 * 			| getMinimalRadius() > 0
 * @invar	A worms maximum action points is equal to its mass rounded to the nearest integer.
 * 			| getMaxActionPoints() == (int)Math.round(getMass())
 * @author 	Thomas De Clercq & Jesse Gielen - 1BA Informatica
 * @gitrepo	https://bitbucket.org/T_DeClercq/ogp-assignment-1-worms
 * @version	1.0
 */

public class Worm {
	
	
	
	/**
	 * Create a new worm at the given location, 
	 * with the given orientation, radius and name.
	 * @post 	getXCoordinate will return xCoordinate.
	 * 			| new.getXCoordinate() == xCoordinate
	 * @post 	getYCoordinate will return yCoordinate.
	 * 			| new.getYCoordinate() == yCoordinate
	 * @effect 	Set the orientation of this worm to be orientation.
	 * 			| this.setOrientation(orientation)
	 * @effect 	Set the radius of this worm to be radius.
	 * 			| this.setRadius(radius)
	 * @effect 	Give this worm the name 'name'.
	 * 			| this.setName(name)
	 * 
	 * @param 	xCoordinate
	 * 			| The x-coordinate (horizontal) of this worm.
	 * @param 	yCoordinate
	 * 			| The y-coordinate (vertical) of this worm.
	 * @param 	orientation
	 * 			| The direction of this worm, expressed as an angle in radians.
				  For example, the angle of a worm facing right is 0, a worm facing up is at
				  angle PI/2, a worm facing left is at angle PI and a worm facing down is at angle
				  3PI/2.
	 * @param 	radius
	 * 			| The radius of this worm (expressed in meters) centered on the worm's position.
	 * @param 	name
	 * 			| The name of this worm.
	 * @throws	IllegalArgumentException
	 * 			| If xCoordinate, yCoordinate, radius or name are invalid.
	 */
	public Worm(double xCoordinate, double yCoordinate, double orientation, double radius, String name) throws IllegalArgumentException {
		this.setXCoordinate(xCoordinate);
		this.setYCoordinate(yCoordinate);
		this.setOrientation(orientation);
		this.setRadius(radius);
		this.setName(name);
		this.setActionPoints(this.getMaxActionPoints());
	}
	
	
	
	// Aspects related to position: Defensively
	
	/**
	 * Get the x coordinate of this worm.
	 * @return	The x coordinate of this worm.
	 */
	@Basic @Raw
	public double getXCoordinate() {
		return this.xCoordinate;
	}
	
	/**
	 * @post 	Set the xCoordinate of this worm to be xCoordinate
	 * 		 	| new.getXCoordinate() == xCoordinate
	 * @param 	xCoordinate The new xCoordinate for this worm.
	 * @throws	IllegalArgumentException	
	 * 			When xCoordinate is not valid.
	 * 			| !isValidXCoordinate(xCoordinate)
	 */
	@Raw
	private void setXCoordinate(double xCoordinate) throws IllegalArgumentException {
		if (!isValidXCoordinate(xCoordinate))
			throw new IllegalArgumentException();
		
		this.xCoordinate = xCoordinate;
	}
	
	/**
	 * Get the y coordinate of this worm
	 * @return	The y coordinate of this worm.
	 */
	@Basic @Raw
	public double getYCoordinate() {
		return this.yCoordinate;
	}
	
	/**
	 * @post 	Set the yCoordinate of this worm to be yCoordinate
	 * 		 	| new.getYCoordinate() == yCoordinate
	 * @param yCoordinate The new yCoordinate for this worm.
	 * @throws	IllegalArgumentException	
	 * 			When yCoordinate is not valid.
	 * 			| !isValidYCoordinate(yCoordinate)
	 */
	@Raw
	private void setYCoordinate(double yCoordinate) throws IllegalArgumentException {
		if (!isValidYCoordinate(yCoordinate))
			throw new IllegalArgumentException();
		
		this.yCoordinate = yCoordinate;
	}
	
	
	/**
	 * Check if the passed double is a valid x coordinate.
	 * @param 	coordinate
	 * 			The coordinate to be checked.
	 * @return	A boolean indicating if the given x coordinate is valid.
	 * 			| result == isValidNumber(coordinate)
	 */
	public static boolean isValidXCoordinate(double coordinate) {
		return isValidNumber(coordinate);
	}
	
	/**
	 * Check if the passed double is a valid y coordinate.
	 * @param 	coordinate
	 * 			The coordinate to be checked.
	 * @return	A boolean indicating if the given y coordinate is valid.
	 * 			| result == isValidNumber(coordinate)
	 */
	public static boolean isValidYCoordinate(double coordinate) {
		return isValidNumber(coordinate);
	}
	
	/**
	 * Get a boolean indicating whether or not this worm can move the given amount of steps.
	 * @param 	nbSteps	
	 * 			The amount of steps to move
	 * @return	A boolean indicating if the worm can move the given amount of steps.
	 * 			| result == (worm.getActionPoints() >= nbSteps * (60/worm.getOrientation()))
	 */
	public boolean canMove(int nbSteps) {
		return this.getActionPoints() >= nbSteps * Actions.MOVE.getRequiredActionPoints(this);
	}
	
	/**
	 * Move the worm in the amount of given steps. 
	 * @param 	nbSteps 
	 * 			The number of steps to move.
	 * @throws 	IllegalArgumentException	
	 * 			When nrOfSteps is not larger than 0
	 * 			| nbSteps <= 0
	 */
	public void move(int nbSteps) throws IllegalArgumentException {
		if (nbSteps <= 0.0)
			throw new IllegalArgumentException();
		
		this.setXCoordinate(this.getXCoordinate() + nbSteps * Math.cos(this.getOrientation()) * this.getRadius());
		this.setYCoordinate(this.getYCoordinate() + nbSteps * Math.sin(this.getOrientation()) * this.getRadius());
				
		this.setActionPoints((int) (this.actionPoints 
				 - nbSteps * Actions.MOVE.getRequiredActionPoints(this)));
	}
	
	private double xCoordinate;
	private double yCoordinate;
	
	// Aspects related to direction: Nominally
	/**
	 * Returns the orientation of the worm.
	 * 
	 * @return	A double with the radian value of the orientation of the worm.
	 * 			| result == worm.orientation
	 */
	@Basic @Raw
	public double getOrientation() {
		return this.orientation;
	}
	
	
	/**
	 * Change the orientation of the worm to the given orientation.
	 * @pre		orientation is a valid orientation
	 * 			| this.isValidorientation(orientation)
	 * @post	The new orientation of this worm is the given orientation.
	 * 			| new.getOrientation() == orientation
	 * @param 	orientation
	 * 			An angle in radians to be the new orientation of the worm.
	 */
	@Raw
	public void setOrientation(double orientation) {	
		assert isValidOrientation(orientation);
		this.orientation = orientation;
	}
	
	
	/**
	 * Checks if the given orientation is a valid orientation for any worm.
	 * @param 	orientation
	 * 			An angle in radians, to be checked if it is a valid orientation.
	 * @return 	True if the orientation is a valid number and larger than or equal to 0 and smaller than 2*PI. False otherwise.
	 * 			| result == (orientation >= 0.0 && orientation < Math.PI * 2.0) 
	 */
	public static boolean isValidOrientation(double orientation) {
		return isValidNumber(orientation) && 
				orientation >= 0.0 &&
				orientation < Math.PI * 2.0;
	}
	
	/**
	 * Returns a boolean that is true if the worm can turn and false if the worm cannot turn.
	 * @param 	angle 
	 * 			The angle to turn.
	 * @return	True if the worm has enough actionPoints to turn the given angle, false otherwise.
	 * 			| result == (worm.getActionPoints() >= 60 * Math.abs(angle/(2*Math.PI)))
	 */
	public boolean canTurn(double angle) {
		return this.getActionPoints() >= 60 * Math.abs(angle/(2*Math.PI));
	}
	
	/**
	 * Turns the worm with given angle.
	 * @pre		Angle is larger than or equal to 0 and smaller than two times pi.
	 * 			| (angle >= 0 && angle < 2* Math.PI)
	 * @post	This worm will turn by the given angle (in radians).
	 * 			| new.getOrientation() == this.setOrientation(this.getOrientation + angle)
	 * 
	 * @param 	angle	
	 * 			The angle the worm should turn (in radians).
	 */
	public void turn(double angle) {
		double newOrientation = this.getOrientation() + angle;
		
		if (newOrientation < 0.0)
			newOrientation = 2.0 * Math.PI - (Math.abs(newOrientation) % (2.0 * Math.PI));
		if (newOrientation >= 2.0 * Math.PI)
			newOrientation %= 2.0 * Math.PI;
		
		// assert isValidOrientation(newOrientation);
		this.setOrientation(newOrientation);
		this.setActionPoints(this.getActionPoints() - (60.0 * Math.abs(angle/(2.0*Math.PI))));
	}
	
	private double orientation;

	// Aspects related to radius: Defensively
	/**
	 * Returns the radius of the worm.
	 * @return 	radius
	 * 			The radius of this worm in meters.
	 * 			| result == worm.radius
	 */
	@Basic @Raw
	public double getRadius() {
		return this.radius;
	}
	
	/**
	 * Sets the radius of the worm to the given amount.
	 * @param radius
	 * @throws 	IllegalArgumentException
	 * 			When radius is not valid.
	 * 			| !isValidRadius(radius)
	 */
	@Raw
	public void setRadius(double radius) throws IllegalArgumentException {
		if (!isValidRadius(radius, this))
			throw new IllegalArgumentException();
		
		this.radius = radius;
		// To prevent action points from surpassing the maximum value
		this.setActionPoints(this.getActionPoints());
	}
	
	/**
	 * Get the minimal possible radius for a worm.
	 * @return 	The minimal radius for this worm.
	 */
	@Basic @Raw
	public double getMinimalRadius() {
		return this.minimalRadius;
	}
	
	/**
	 * Check whether a worm can have a radius equal to the amount given.
	 * @return	A boolean indicating if radius is a valid radius for the given worm.
	 * 			| result == (isValidNumber(radius) && radius >= worm.getMinimalRadius())
	 */
	public static boolean isValidRadius(double radius, Worm worm) {
		return isValidNumber(radius) 
				&& radius >= worm.minimalRadius;
	}
	
	private double radius;
	private double minimalRadius = 0.25;
	
	// Aspects related to mass: Defensively
	/**
	 * Getter for the mass of a worm.
	 * @return  The mass of this worm.
	 * 			| (DENSITY * (4.0/3.0 * Math.PI * Math.pow(worm.getRadius(), 3)))
	 */
	public double getMass() {
		return DENSITY * (4.0/3.0 * Math.PI * Math.pow(this.getRadius(), 3));
	}

	private static final double DENSITY = 1062.0;
	
	// Aspects related to action points: Total
	
	/**
	 * Set the actionpoints of a worm to a given amount floored to the nearest integer.
	 * @param actionPoints	The desired amount of actionpoints. 
	 * 						The provided double gets floored to an integer.
	 * @effect	Set the actionpoints to be actionPoints floored to the nearest integer.
	 * 			| this.setActionPoints((int)Math.floor(actionPoints))
	 */
	private void setActionPoints(double actionPoints) {
		int flooredActionPoints = (int)Math.floor(actionPoints);
		setActionPoints(flooredActionPoints);
	}
	
	
	/**
	 * Set the actionpoints of a worm to the given amount.
	 * @param 	actionPoints	
	 * 			The desired amount of actionpoints.
	 * @post 	If actionPoints is a valid amount of actionpoints, the actionpoints
	 * 			of this worm will be set to actionPoints.
	 * 			| if (isValidActionPoints(actionPoints, this))
	 * 				new.getActionPoints() == actionPoints
	 * @post	If actionPoints is smaller than zero, the actionpoints
	 * 			of this worm will be set to zero.
	 * 			| if (actionPoints < 0)
	 * 				new.getActionPoints() == 0
	 * @post	If actionPoints is larger than the maximum amount allowed,
	 * 			the actionpoints of this worm will be set to the maximum.
	 * 			| if (actionPoints > this.getMaxActionPoints())
	 * 				new.getActionPoints() == this.getMaxActionPoints()
	 */
	private void setActionPoints(int actionPoints) {
		actionPoints = Math.max(0, actionPoints);
		actionPoints = Math.min(this.getMaxActionPoints(), actionPoints);
		
		this.actionPoints = actionPoints;
	}
	
	
	/**
	 * Get the number of actionpoints for this worm.
	 * @return 	The current number of action points.
	 */
	@Basic @Raw
	public int getActionPoints() {
		return this.actionPoints;
	}
	
	/**
	 * Check whether a given amount of actionpoints is a valid amount for the given worm.
	 * @param actionPoints 	The amount of action points to check for.
	 * @param worm			The worm to check on.
	 * @return True if the amount of actionpoints equals a value between 0 and the maximum actionpoints for this worm. False otherwise.
	 */
	public static boolean isValidActionPoints(int actionPoints, Worm worm) {
		return actionPoints >= 0 && actionPoints <= worm.getMaxActionPoints();
	}
	
	/**
	 * Get the maximum amount of actionpoints possible.
	 * @return	The maximum number of action points which is equal to the mass rounded to the nearest integer.
	 * 			| result == Math.round(this.getMass())
	 */
	public int getMaxActionPoints() {
		return (int) Math.round(this.getMass());
	}
	
	private int actionPoints;
	
	
	
	
	
	/**
	 * Check whether a given name is a valid name for a worm.
	 * @param 	name	The name to check.
	 * @return 	True if the name is valid, false otherwise. Returns true only if the name is
	 *			at least two characters long and starts with an uppercase letter. [In the
	 *			current version, names can only use letters (both uppercase and lowercase),
	 *			quotes (both single and double) and spaces.]
	 */
	public static boolean isValidName(String name) {
		boolean areValidChars = true;
		
		for (int i = 0; i < name.length(); i++) {
			char checkingChar = name.charAt(i);
			if (!isValidChar(checkingChar)) {
				areValidChars = false;
				break;
			}			
		}
		return (name.length() >= 2 && Character.isUpperCase(name.charAt(0)) && 
				areValidChars);
	}

	/**
	 * Check if the given char is valid in a worms name.
	 * @param 	checkingChar
	 * 			The character to check.
	 * @return	A boolean indicating whether the checkingChar is a legal char for a worm name.
	 */
	private static boolean isValidChar(char checkingChar) {
		return (checkingChar >= 'A' && checkingChar <= 'Z') ||
		(checkingChar >= 'a' && checkingChar <= 'z')
		|| legalChars.contains(String.valueOf(checkingChar));
	}	
	
	private static String legalChars = " '\"";
	
	/**
	 * Get the name for this worm.
	 * @return 	The name of the worm.
	 * 			| result == worm.name
	 */
	@Basic @Raw
	public String getName() {
		return this.name;
	}
	
	/**
	 * Set the name of this worm to be the passed name.
	 * @param name	The new name for this worm.
	 * @post	The worms name will be name.
	 * 			| new.getName() == name
	 * @throws 	IllegalArgumentException
	 * 			Name is not valid.
	 * 			| !isValidName(name);
	 */
	@Raw
	public void setName(String name) throws IllegalArgumentException {
		if (!isValidName(name))
			throw new IllegalArgumentException();
		
		this.name = name;
	}
	
	private String name;
	
	/**
	 * @return	The force exerted on the worm when it jumps.
	 */
	private double getWormForce() {
		return (5.0 * this.getActionPoints()) + (this.getMass() * standardAccel);	
	}
	
	/**
	 * @return	The initial velocity when a worm jumps.
	 */
	private double getStartingSpeed(){
		return (getWormForce() / this.getMass()) * 0.5;
	}
	
	/**
	 * Makes the given worm jump.
	 * @pre	The actionpoints of a worm should be higher than 0.
	 * 		| getActionpoints > 0
	 * @pre The orientation of a worm should be between Pi and two times Pi.
	 * 		| getOrientation > Math.PI && worm.orientation < 2*Math.PI
	 * @post	The jump distance gets added to the worms x coordinate.
	 * 			| new.getXCoordinate() == this.getXCoordinate() + this.getDistance()
	 * @post	This worm has zero actionpoints left.
	 * 			| new.getActionPoints == 0
	 * @throws 	IllegalStateException
	 * 			This worm doesn't have any actionpoints or it is facing downward.
	 * 			| this.getActionPoints() <= 0 || 
	 * 			| (this.getOrientation() > Math.PI && this.getOrientation() < (2*Math.PI))
	 */
	public void jump() throws IllegalStateException {
		if (this.getActionPoints() <= 0)
			throw new IllegalStateException();
		if (this.getOrientation() > Math.PI && this.getOrientation() < (2*Math.PI))
			throw new IllegalStateException();
		
		this.setXCoordinate(this.getXCoordinate() +  this.getDistance());
		this.setActionPoints(0);
	}
	
	
	/**
	 * Get the horizontal distance that this worm will jump
	 * @return	The horizontal distance that this worm will jump.
	 * 			| (Math.pow(getStartingSpeed(), 2) 
	 * 			| * Math.sin(2.0 * getOrientation())) / standardAccel
	 */
	public double getDistance(){
		double distance = (Math.pow(getStartingSpeed(), 2) 
				 * Math.sin(2.0 * this.getOrientation())) / standardAccel;
		return distance;
	}
	/**
	 * Returns the total amount of time (in seconds) that a
	 * jump of the given worm would take.
	 * Returns 0 when a worm has no actionpoints left.
	 * @return	Amount of time in seconds a jump of this worm would take.
	 * 			| result == getDistance()/(getStartingSpeed() * Math.cos(orientation));
	 */
	public double getJumpTime(){
		if (this.getActionPoints() == 0)
			return 0;
		
		return getDistance()/(getStartingSpeed() * Math.cos(this.getOrientation()));
	}
	
	/**
	 * Returns the location on the jump trajectory of the given worm
	 * after a time t.
	 *  
	 * @param 	deltaT 
	 * 			The time to get the position for in the jump (in seconds).
	 * @return 	An array with two elements,
	 *  		with the first element being the x-coordinate and
	 *  		the second element the y-coordinate
	 *  		| result == [this.xCoordinate + (xSpeed * deltaT), this.yCoordinate + (ySpeed * deltaT - (1.0/2.0) * standardAccel * Math.pow(deltaT, 2))]
	 */
	public double[] getJumpStep(double deltaT) throws IllegalArgumentException {
		
		double[] computedPosition = new double[2];
		
		double xSpeed =  getStartingSpeed() * Math.cos(getOrientation());
		double ySpeed = getStartingSpeed() * Math.sin(getOrientation());
		double xDeltaT = this.xCoordinate + (xSpeed * deltaT);
		double yDeltaT = this.yCoordinate + (ySpeed * deltaT - (1.0/2.0) * standardAccel * Math.pow(deltaT, 2));
		
		computedPosition[0] = xDeltaT;
		computedPosition[1] = yDeltaT;
	    return computedPosition; 
	}
	
	/**
	 * The standard acceleration on earth, used for calculations involving physics.
	 */
	private final double standardAccel =  9.80665;
	
	
	/**
	 * @return	True if the number is valid, false otherwise.
	 * 			| result == !Double.isNaN(number)
	 */
	public static boolean isValidNumber(double number) {
		return !Double.isNaN(number);
	}
	
}
